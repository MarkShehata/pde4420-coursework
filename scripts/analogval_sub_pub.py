#!/usr/bin/env python
import rospy
from mini_project.msg import analogData

pub = rospy.Publisher('servo_out_topic', analogData, queue_size=10)
servoAngles = analogData()

def remap(in_val, in_min, in_max, out_min, out_max):
  return (in_val - in_min) * (out_max - out_min) / (in_max - in_min) + out_min

def callback(xy_data):
    # remapping function
    global servoAngles
    servoAngles.x_value = remap(xy_data.x_value, 0, 1024, 0, 180)
    servoAngles.y_value = remap(xy_data.y_value, 0, 1024, 0, 180)
    rospy.loginfo(servoAngles)
    pub.publish(servoAngles)

def js_servo():
    rospy.init_node('joyIn_servoOut', anonymous=True)
    rospy.Subscriber('joy_in_topic', analogData, callback)
    rospy.spin()

if __name__ == '__main__':
    js_servo()