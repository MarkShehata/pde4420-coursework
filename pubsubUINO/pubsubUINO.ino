/*
   rosserial PubSub Example
   Prints "hello world!" and toggles led
*/

#include <ros.h>
#include <Servo.h>
#include <std_msgs/Int16.h>
#include <mini_project/analogData.h>

ros::NodeHandle  nh;
Servo servo9;
Servo servo10;

void servoCb(mini_project::analogData& servoAngles) {
  servo9.write(servoAngles.x_value);
  servo10.write(servoAngles.y_value);
}

ros::Subscriber<mini_project::analogData> sub("servo_out_topic", servoCb);

mini_project::analogData joyStick_values;
ros::Publisher pub("joy_in_topic", &joyStick_values);

void setup()
{
  servo9.attach(9); //attach it to pin 9
  servo10.attach(10); //attach it to pin 9
  nh.initNode();
  nh.advertise(pub);
  nh.subscribe(sub);
}

void loop()
{
  joyStick_values.x_value = analogRead(A0);
  joyStick_values.y_value = analogRead(A1);
  pub.publish( &joyStick_values );
  nh.spinOnce();
}
